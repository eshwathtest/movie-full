import React from "react";
import { useEffect, useState } from "react";
import { Formik } from "formik";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import io from "socket.io-client";
import "./ChatRoomPage.css";
import { getChatRoomMessages, getChatRooms } from "./requests";
import VideoPlayer from './VideoPlayer'
const SOCKET_IO_URL = "http://localhost:3000";
const socket = io(SOCKET_IO_URL);
const getChatData = () => {
  return JSON.parse(localStorage.getItem("chatData"));
};

const schema = yup.object({
  message: yup.string().required("Message is required"),
});

function ChatRoomPage() {
  const [initialized, setInitialized] = useState(false);
  const [messages, setMessages] = useState([]);
  const [rooms, setRooms] = useState([]);
  const [videoUrl, setUrl] = useState('');
  const [videoPlaybackTime, SetPlayBackTime] = useState(0);

  const handleSubmit = async evt => {
    const isValid = await schema.validate(evt);
    if (!isValid) {
      return;
    }
    const data = Object.assign({}, evt);
    data.chatRoomName = getChatData().chatRoomName;
    data.author = getChatData().handle;
    data.message = evt.message;
    data.syncTime = window.newPlayer.currentTime()
    console.log("sent data", data)
    socket.emit("message", data);
  };

  const connectToRoom = () => {
    socket.on("connect", data => {
      socket.emit("join", getChatData().chatRoomName);
    });

    socket.on("newMessage", data => {
      getMessages();
    });
    setInitialized(true);
  };

  const getMessages = async () => {
    const response = await getChatRoomMessages(getChatData().chatRoomName);
    setMessages(response.data);
    console.log("from message", response.data)
    debugger
    if(response.data.length > 1 && response.data[response.data.length - 1].message === 'sync'){
      window.newPlayer.currentTime(response.data[response.data.length - 1].syncTime)
    }
    setInitialized(true);
  };

  const getRooms = async () => {
    const response = await getChatRooms();
    setRooms(response.data);
    if(response.data){
      setUrl(response.data[response.data.length - 1].url)
    }
    setInitialized(true);
  };

  useEffect(() => {
    if (!initialized) {
      getMessages();
      connectToRoom();
      getRooms();
    }
  });

  return (
    <div className="chat-room-page" style={{display: "flex"}}>
      <div className="video-player">
      {videoUrl && initialized ?
        <VideoPlayer videoUrl={videoUrl} />
        : null }
      </div>
      <div className="chat-player">
        <h1>
          Chat - {getChatData().chatRoomName}
        </h1>
        <div className="chat-box">
          {messages.map((m, i) => {
            return (
              <div className="col-12" key={i}>
                <div>
                  <div className="col-2"><b>{m.author}</b></div>
                  <div className="col">{m.message}</div>
                </div>
              </div>
            );
          })}
        </div>
        <Formik validationSchema={schema} onSubmit={handleSubmit}>
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isInvalid,
            errors,
          }) => (
              <Form noValidate onSubmit={handleSubmit} style={{margin: "20px"}}>
                <Form.Row>
                  <Form.Group as={Col} md="12" controlId="handle">
                    <Form.Label>Message</Form.Label>
                    <Form.Control
                      type="text"
                      name="message"
                      placeholder="Message"
                      value={values.message || ""}
                      onChange={handleChange}
                      isInvalid={touched.message && errors.message}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.message}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Form.Row>
                <Button type="submit" style={{ marginRight: "10px" }}>
                  Send
            </Button>
              </Form>
            )}
        </Formik>
      </div>
    </div>
  );
}

export default ChatRoomPage;
